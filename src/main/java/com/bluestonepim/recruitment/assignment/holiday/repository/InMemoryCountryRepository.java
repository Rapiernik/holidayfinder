package com.bluestonepim.recruitment.assignment.holiday.repository;

import java.util.HashSet;
import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryCountryRepository implements CountryRepository {

    private final Set<Country> countries = new HashSet<>();

    @Override
    public void saveAll(Set<Country> newCountries) {
        countries.addAll(newCountries);
    }

    @Override
    public Set<Country> findAll() {
        return new HashSet<>(countries);
    }
}

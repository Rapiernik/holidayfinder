package com.bluestonepim.recruitment.assignment.holiday.model;

import java.time.LocalDate;
import java.util.List;

public class HolidayResponse {

    private final LocalDate date;
    private final List<Holiday> holidays;

    public HolidayResponse(LocalDate date,
            List<Holiday> holidays) {
        this.date = date;
        this.holidays = holidays;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<Holiday> getHolidays() {
        return holidays;
    }
}

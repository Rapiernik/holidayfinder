package com.bluestonepim.recruitment.assignment.holiday.controller;

import static com.bluestonepim.recruitment.assignment.holiday.util.ApplicationUtils.splitComaSeparatedValues;

import javax.validation.constraints.Pattern;

import java.time.LocalDate;

import com.bluestonepim.recruitment.assignment.holiday.model.HolidayResponse;
import com.bluestonepim.recruitment.assignment.holiday.service.HolidayService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("holidays")
@Validated
public class HolidayController {

    private final HolidayService holidayService;

    private static final String COUNTRY_CODES_PATTERN = "^([A-Za-z]){2}+([,][A-Za-z]{2})*$";
    private static final String INVALID_COUNTRY_CODES_PATTERN_MSG =
            "Invalid countries pattern: (good: PL,FR), (bad: PLFR or PL,FR,)";

    private static final String DATE_PATTERN = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$";
    private static final String INVALID_DATE_PATTERN_MSG = "Invalid date pattern, the valid one is: yyyy-MM-dd";

    public HolidayController(HolidayService holidayService) {
        this.holidayService = holidayService;
    }

    @GetMapping("next/{countryCodes}/{date}")
    public HolidayResponse getNextHoliday(
            @PathVariable @Pattern(regexp = COUNTRY_CODES_PATTERN, message = INVALID_COUNTRY_CODES_PATTERN_MSG)
            final String countryCodes,
            @PathVariable @Pattern(regexp = DATE_PATTERN, message =
                    INVALID_DATE_PATTERN_MSG) final String date) {

        return holidayService.getNextHoliday(splitComaSeparatedValues(countryCodes), LocalDate.parse(date), null);
    }

}


package com.bluestonepim.recruitment.assignment.holiday.validation;

import static java.util.stream.Collectors.toList;

import javax.validation.ValidationException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import com.bluestonepim.recruitment.assignment.holiday.repository.CountryRepository;
import org.springframework.stereotype.Component;

@Component
public class CountriesValidator {

    private final CountryRepository repository;

    public CountriesValidator(CountryRepository repository) {
        this.repository = repository;
    }

    public void validate(List<String> countryCodes) {
        Set<Country> availableCountries = repository.findAll();
        Set<String> availableCountryCodes = availableCountries
                .stream()
                .map(Country::getCountryCode)
                .collect(Collectors.toSet());

        if (countryCodes.size() < 2) {
            throw new ValidationException("Please provide at least two countries");
        }
        List<String> upperCaseValues = countryCodes
                .stream()
                .map(String::toUpperCase)
                .collect(toList());
        if (!availableCountryCodes.containsAll(upperCaseValues)) {
            throw new ValidationException("One or more countries provided are not supported by external API");
        }
    }
}

package com.bluestonepim.recruitment.assignment.holiday.model;

import java.time.LocalDate;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Holiday {

    private final LocalDate date;
    private final String name;
    private final String countryCode;

    public Holiday(LocalDate date, String name, String countryCode) {
        this.date = date;
        this.name = name;
        this.countryCode = countryCode;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Holiday that = (Holiday) o;
        return getDate().equals(that.getDate()) &&
                getCountryCode().equals(that.getCountryCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getCountryCode());
    }
}

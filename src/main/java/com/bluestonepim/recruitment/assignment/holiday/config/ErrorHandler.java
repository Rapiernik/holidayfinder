package com.bluestonepim.recruitment.assignment.holiday.config;

import static java.lang.String.format;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

    @ExceptionHandler(ValidationException.class)
    private ResponseEntity<?> handleValidationException(Exception exception) {
        logError(exception);

        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }


    private void logError(Exception ex) {
        LOGGER.error(format("Message: %s", ex.getMessage()));
    }

}

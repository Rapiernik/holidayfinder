package com.bluestonepim.recruitment.assignment.holiday.service;

import static java.lang.String.format;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import com.bluestonepim.recruitment.assignment.holiday.model.Holiday;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Nager.Date - Public Holiday API
 * <p>
 * https://date.nager.at/Api
 */
@Service
public class NagerDateHolidayService implements HolidayDataProvider {

    private final ExternalRequestService requestService;
    private final ObjectMapper objectMapper;

    @Value("${public.holidays.api.url}")
    private String baseUrl;

    public NagerDateHolidayService(ExternalRequestService requestService, ObjectMapper objectMapper) {
        this.requestService = requestService;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<Holiday> getHolidayData(int year, String countryCode) {
        String holidaysUrl = createUrl(countryCode.toUpperCase(), year);
        try {
            byte[] byteResponse = requestService.sendGetRequest(holidaysUrl);
            Object[] objectResponse = objectMapper.readValue(byteResponse, Object[].class);
            return objectMapper.convertValue(objectResponse, new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new RuntimeException("Cannot get holiday data from external API");
        }
    }

    @Override
    public Set<Country> getAvailableCountries() {
        try {
            byte[] byteResponse = requestService.sendGetRequest(baseUrl + "/AvailableCountries");
            Object[] objectResponse = objectMapper.readValue(byteResponse, Object[].class);
            return objectMapper.convertValue(objectResponse, new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new RuntimeException("Cannot get available countries data from external API");
        }
    }

    private String createUrl(String countryCode, int year) {
        return format("%s/%s/%s/%s", baseUrl, "publicholidays", year, countryCode);
    }
}

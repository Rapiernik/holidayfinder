package com.bluestonepim.recruitment.assignment.holiday.repository;

import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;

public interface CountryRepository {

    void saveAll(Set<Country> countries);

    Set<Country> findAll();
}

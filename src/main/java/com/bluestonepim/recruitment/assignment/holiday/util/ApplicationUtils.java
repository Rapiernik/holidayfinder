package com.bluestonepim.recruitment.assignment.holiday.util;

import static java.util.Arrays.asList;

import java.util.List;

public class ApplicationUtils {

    public static List<String> splitComaSeparatedValues(String values) {
        return asList(values.split(","));
    }
}

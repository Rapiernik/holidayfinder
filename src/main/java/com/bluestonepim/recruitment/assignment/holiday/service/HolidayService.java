package com.bluestonepim.recruitment.assignment.holiday.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.bluestonepim.recruitment.assignment.holiday.model.Holiday;
import com.bluestonepim.recruitment.assignment.holiday.model.HolidayResponse;
import com.bluestonepim.recruitment.assignment.holiday.validation.CountriesValidator;
import org.springframework.stereotype.Service;

@Service
public class HolidayService {

    private final HolidayDataProvider holidayDataProvider;
    private final CountriesValidator countriesValidator;

    public HolidayService(HolidayDataProvider holidayDataProvider, CountriesValidator countriesValidator) {
        this.holidayDataProvider = holidayDataProvider;
        this.countriesValidator = countriesValidator;
    }

    public HolidayResponse getNextHoliday(List<String> countryCodes, LocalDate date, Integer year) {

        countriesValidator.validate(countryCodes);

        Set<Holiday> holidays = new HashSet<>();
        int resolvedYear = year != null ? year : date.getYear();
        for (String countryCode : countryCodes) {
            holidays.addAll(holidayDataProvider.getHolidayData(resolvedYear, countryCode));
        }
        List<Holiday> result = findTheNextHolidayAfterGivenDate(holidays, date, countryCodes.size());

        if (result.isEmpty()) {
            return getNextHoliday(countryCodes, LocalDate.of(resolvedYear, Month.DECEMBER, 31), ++resolvedYear);
        } else {
            return new HolidayResponse(result.get(0).getDate(), result);
        }

    }

    private List<Holiday> findTheNextHolidayAfterGivenDate(Set<Holiday> countryCodeToHolidayEntriesMap,
            LocalDate date, int countriesNumber) {

        LinkedHashMap<LocalDate, List<Holiday>> orderedHolidaysMap = countryCodeToHolidayEntriesMap
                .stream()
                .collect(Collectors.groupingBy(Holiday::getDate, Collectors.toList()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return orderedHolidaysMap.keySet()
                .stream()
                .filter(holidayDate -> holidayDate.isAfter(date))
                .filter(holidayDate -> orderedHolidaysMap.get(holidayDate).size() == countriesNumber)
                .findFirst()
                .map(orderedHolidaysMap::get).orElse(List.of());

    }

}

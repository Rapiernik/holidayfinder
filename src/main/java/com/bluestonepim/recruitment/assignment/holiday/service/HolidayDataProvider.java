package com.bluestonepim.recruitment.assignment.holiday.service;

import java.util.List;
import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import com.bluestonepim.recruitment.assignment.holiday.model.Holiday;

public interface HolidayDataProvider {

    List<Holiday> getHolidayData(int year, String countryCode);

    Set<Country> getAvailableCountries();

}

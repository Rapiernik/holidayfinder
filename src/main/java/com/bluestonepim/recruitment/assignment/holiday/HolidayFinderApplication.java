package com.bluestonepim.recruitment.assignment.holiday;

import static java.lang.String.format;

import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import com.bluestonepim.recruitment.assignment.holiday.repository.CountryRepository;
import com.bluestonepim.recruitment.assignment.holiday.service.HolidayDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HolidayFinderApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(HolidayFinderApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(HolidayFinderApplication.class, args);
    }

    @Bean
    public CommandLineRunner init(CountryRepository repository, HolidayDataProvider dataProvider) {
        return (args) -> {
            Set<Country> availableCountries = dataProvider.getAvailableCountries();
            repository.saveAll(availableCountries);
            LOGGER.info(format("Supported countries loaded into repository - %s countries", availableCountries.size()));
        };
    }
}

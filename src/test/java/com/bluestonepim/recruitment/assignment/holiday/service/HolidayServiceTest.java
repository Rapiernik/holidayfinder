package com.bluestonepim.recruitment.assignment.holiday.service;

import static java.time.Month.APRIL;
import static java.time.Month.DECEMBER;
import static java.time.Month.JANUARY;
import static java.time.Month.NOVEMBER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

import com.bluestonepim.recruitment.assignment.holiday.model.Holiday;
import com.bluestonepim.recruitment.assignment.holiday.model.HolidayResponse;
import com.bluestonepim.recruitment.assignment.holiday.validation.CountriesValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HolidayServiceTest {

    @Mock
    private CountriesValidator countriesValidator;

    @Mock
    private HolidayDataProvider dataProvider;

    @InjectMocks
    private HolidayService holidayService;

    @Test
    void shouldReturnNextHoliday() {

        doNothing().when(countriesValidator).validate(anyList());
        when(dataProvider.getHolidayData(eq(2022), eq("PL"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2022, JANUARY, 1), "New Year's Day", "PL"),
                        new Holiday(LocalDate.of(2022, JANUARY, 6), "Epiphany", "PL"),
                        new Holiday(LocalDate.of(2022, APRIL, 17), "Easter Sunday", "PL"),
                        new Holiday(LocalDate.of(2022, APRIL, 18), "Easter Monday", "PL")
                ));

        when(dataProvider.getHolidayData(eq(2022), eq("FR"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2022, JANUARY, 1), "New Year's Day", "FR"),
                        new Holiday(LocalDate.of(2022, APRIL, 18), "Easter Monday", "FR")
                ));

        HolidayResponse nextHolidayResponse =
                holidayService.getNextHoliday(List.of("PL", "FR"), LocalDate.of(2022, JANUARY, 7), null);

        HolidayResponse expectedHolidayResponse = new HolidayResponse(
                LocalDate.of(2022, APRIL, 18),
                List.of(
                        new Holiday(LocalDate.of(2022, APRIL, 18), "Easter Monday", "PL"),
                        new Holiday(LocalDate.of(2022, APRIL, 18), "Easter Monday", "FR")
                )
        );

        assertThat(nextHolidayResponse)
                .usingRecursiveComparison()
                .isEqualTo(expectedHolidayResponse);
    }

    @Test
    void shouldReturnNextHolidayFromNextYear() {

        doNothing().when(countriesValidator).validate(anyList());
        when(dataProvider.getHolidayData(eq(2022), eq("PL"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2022, JANUARY, 1), "New Year's Day", "PL"),
                        new Holiday(LocalDate.of(2022, APRIL, 17), "Easter Sunday", "PL"),
                        new Holiday(LocalDate.of(2022, NOVEMBER, 11), "Independence Day", "PL")
                ));

        when(dataProvider.getHolidayData(eq(2023), eq("PL"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2023, JANUARY, 1), "New Year's Day", "PL"),
                        new Holiday(LocalDate.of(2023, APRIL, 9), "Easter Sunday", "PL")
                ));

        when(dataProvider.getHolidayData(eq(2022), eq("FR"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2022, JANUARY, 1), "New Year's Day", "FR"),
                        new Holiday(LocalDate.of(2022, APRIL, 18), "Easter Monday", "FR")
                ));

        when(dataProvider.getHolidayData(eq(2023), eq("FR"))).thenReturn(
                List.of(
                        new Holiday(LocalDate.of(2023, JANUARY, 1), "New Year's Day", "FR"),
                        new Holiday(LocalDate.of(2023, APRIL, 9), "Easter Monday", "FR")
                ));

        HolidayResponse nextHolidayResponse =
                holidayService.getNextHoliday(List.of("PL", "FR"), LocalDate.of(2022, DECEMBER, 27), null);

        HolidayResponse expectedHolidayResponse = new HolidayResponse(
                LocalDate.of(2023, JANUARY, 1),
                List.of(
                        new Holiday(LocalDate.of(2023, JANUARY, 1), "New Year's Day", "PL"),
                        new Holiday(LocalDate.of(2023, JANUARY, 1), "New Year's Day", "FR")
                )
        );

        assertThat(nextHolidayResponse)
                .usingRecursiveComparison()
                .isEqualTo(expectedHolidayResponse);
    }
}

package com.bluestonepim.recruitment.assignment.holiday.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import com.bluestonepim.recruitment.assignment.holiday.model.Holiday;
import com.bluestonepim.recruitment.assignment.holiday.model.HolidayResponse;
import com.bluestonepim.recruitment.assignment.holiday.service.HolidayService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class HolidayControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HolidayService holidayService;

    @Test
    void shouldReturnNextHolidayDate() throws Exception {
        LocalDate returnedDate = LocalDate.of(2022, Month.DECEMBER, 26);
        when(holidayService.getNextHoliday(anyList(), any(), eq(null))).thenReturn(
                new HolidayResponse(
                        returnedDate,
                        List.of(
                                new Holiday(returnedDate, "St. Stephen's Day", "PL"),
                                new Holiday(returnedDate, "St. Stephen's Day", "DE")
                        )));

        mockMvc
                .perform(get("/holidays/next/pL,de,gb/2022-05-27"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.date", is("2022-12-26")))
                .andExpect(jsonPath("$.holidays[0].date", is("2022-12-26")))
                .andExpect(jsonPath("$.holidays[0].name", is("St. Stephen's Day")))
                .andExpect(jsonPath("$.holidays[0].countryCode", is("PL")))
                .andExpect(jsonPath("$.holidays[1].date", is("2022-12-26")))
                .andExpect(jsonPath("$.holidays[1].name", is("St. Stephen's Day")))
                .andExpect(jsonPath("$.holidays[1].countryCode", is("DE")));

    }

    @Test
    void shouldReturnBadRequestWhenProvideInvalidDateFormat() throws Exception {

        mockMvc
                .perform(get("/holidays/next/pL,de,gb/00-05-27"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Invalid date pattern, the valid one is: yyyy-MM-dd")));

    }

    @Test
    void shouldReturnBadRequestWhenProvideInvalidCodesFormat() throws Exception {

        mockMvc
                .perform(get("/holidays/next/pL,de,/2022-05-27"))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .string(containsString("Invalid countries pattern: (good: PL,FR), (bad: PLFR or PL,FR,)")));

    }
}

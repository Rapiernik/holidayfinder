package com.bluestonepim.recruitment.assignment.holiday.validation;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import javax.validation.ValidationException;

import java.util.List;
import java.util.Set;

import com.bluestonepim.recruitment.assignment.holiday.model.Country;
import com.bluestonepim.recruitment.assignment.holiday.repository.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CountriesValidatorTest {

    @Mock
    private CountryRepository repository;

    @InjectMocks
    private CountriesValidator validator;

    @Test
    void shouldPassValidation() {
        initMock();

        assertThatCode(() -> validator.validate(List.of("PL", "FR"))).doesNotThrowAnyException();
    }

    @Test
    void shouldFailValidationWhenOneCountryIsNotSupported() {
        initMock();

        assertThatThrownBy(() -> {
            validator.validate(List.of("PL", "DE"));
        })
                .isInstanceOf(ValidationException.class)
                .hasMessage("One or more countries provided are not supported by external API");
    }

    @Test
    void shouldFailValidationWhenJustOneCountryProvided() {
        initMock();

        assertThatThrownBy(() -> {
            validator.validate(List.of("PL"));
        })
                .isInstanceOf(ValidationException.class)
                .hasMessage("Please provide at least two countries");
    }

    private void initMock() {
        when(repository.findAll()).thenReturn(Set.of(
                new Country("PL", "Poland"),
                new Country("FR", "France"),
                new Country("RU", "Russia")
        ));
    }

}

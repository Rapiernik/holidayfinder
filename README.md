## Holiday Finder Service

#### Description
Service accepts at least two country codes and a date in a format YYYY-MM-DD, then return next holiday date after the given date that will happen on the same day in all countries. 

Both parameters passed in path variable. Country codes in following form: "PL,FR,DE" (commas important, no spaces between, case insensitive)

#### API Documentation
http://localhost:8080/swagger-ui/index.html

#### Running tests
mvn test

#### Running application
##### Docker
- docker-compose up

By default 8080 port is used by application, feel free to change it in ".env" file if 8080 is already in use in your machine.

##### alternatively Maven
- mvn clean package
- mvn spring-boot:run
